//
//  BannerTableViewCell.swift
//  Banka Kurlari
//
//  Created by Atakan Cengiz KURT on 18.05.2023.
//  Copyright © 2023 Atakan Cengiz KURT. All rights reserved.
//

import GoogleMobileAds
import UIKit

class BannerTableViewCell: UITableViewCell {

    @IBOutlet weak var bannerCellView: GADBannerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bannerCellView.adUnitID = "ca-app-pub-1299028299020213/5814557473" //"ca-app-pub-3940256099942544/2934735716"
        bannerCellView.load(GADRequest())
    }
    
}
