//
//  PriceBankTableViewCell.swift
//  Banka Makas Araligi
//
//  Created by MacBook Pro on 24.05.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class PriceBankTableViewCell: UITableViewCell {

    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var buyPrice: UILabel!
    @IBOutlet weak var sellPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
