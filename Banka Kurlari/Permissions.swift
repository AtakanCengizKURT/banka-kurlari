//
//  Permissions.swift
//  Banka Kurlari
//
//  Created by Atakan Cengiz KURT on 8.11.2021.
//  Copyright © 2021 Atakan Cengiz KURT. All rights reserved.
//

import Foundation
import AppTrackingTransparency
import UserNotifications

struct Permissons{

    
    static func setNotification(){
        //Ask for notification permission
        let n = NotificationHandler()
        n.askNotificationPermission {

            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if #available(iOS 14, *) {
                  ATTrackingManager.requestTrackingAuthorization { (status) in
                    //print("IDFA STATUS: \(status.rawValue)")
                    //FBAdSettings.setAdvertiserTrackingEnabled(true)
                  }
                }
            }
        }
    }
}


class NotificationHandler{
//Permission function
func askNotificationPermission(completion: @escaping ()->Void){
    
    //Permission to send notifications
    let center = UNUserNotificationCenter.current()
    // Request permission to display alerts and play sounds.
    center.requestAuthorization(options: [.alert, .badge, .sound])
    { (granted, error) in
        // Enable or disable features based on authorization.
        completion()
    }
}
}
