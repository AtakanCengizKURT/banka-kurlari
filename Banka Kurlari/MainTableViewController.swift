//
//  MainTableViewController.swift
//  Banka Makas Araligi
//
//  Created by MacBook Pro on 24.05.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import GoogleMobileAds

var globalCounter = 0


class MainTableViewController: UITableViewController, GADFullScreenContentDelegate, GADBannerViewDelegate {
    var interstitial: GADInterstitialAd!
    
    enum Sort{
        case bankName
        case scissorsRange
        case buyPriceDecreasing
        case buyPriceGrowing
        case salesPriceDecreasing
        case salesPriceGrowing
    }
    
    var sort = Sort.bankName{
        didSet{
            getPriceBank(sort)
        }
    }
   
    let turkish = Locale(identifier: "tr")
    var currentTime: String = ""
    var currencyUnit = Currency.usd
    @IBOutlet weak var btnCurrency: UIBarButtonItem!
    
    var listOfPriceBanks = [PriceBankDetail](){
        didSet {
            DispatchQueue.main.async {
                self.getTime()
                self.tableView.reloadData()
                self.navigationItem.title = "Güncellenme Saati: \(self.currentTime)"
            }
        }
    }
    
    @objc func endEditing(){
        self.view.endEditing(true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Permissons.setNotification()
        interstitialBanner()
        btnCurrency.image = UIImage(named: currencyUnit.getCurrencyString())
        sort = Sort.bankName
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        self.tableView.register(UINib(nibName: "BannerTableViewCell", bundle: nil), forCellReuseIdentifier: "bannerCell")
   
    }
    
    func showInterstitial(){
        if interstitial != nil {
            if globalCounter > 1{
                globalCounter = 0
                interstitial.present(fromRootViewController: self)
            }else {
                globalCounter += 1
            }
        }
    }
    

    
    func interstitialBanner(){
        let request = GADRequest()

        GADInterstitialAd.load(withAdUnitID:"ca-app-pub-1299028299020213/2800777174",
                               request: request,
                               completionHandler: { [self] ad, error in
            if let error = error {
                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                return
            }
            interstitial = ad
            interstitial?.fullScreenContentDelegate = self
        }
        )
    }

    
    
    func getTime(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        self.currentTime = formatter.string(from: date)
    }
    
    @IBAction func refresh(_ sender: UIRefreshControl) {
        getPriceBank(self.sort)
        self.getTime()
        sender.attributedTitle = NSAttributedString(string: "\(self.currentTime)")
        sender.endRefreshing()
        
    }
    
    
    func getPriceBank(_ sort: Sort){
        self.navigationItem.title = "Güncelleniyor..."
        print("getPriceBank")
        let priceBankRequest = PriceBankRequest(currencyUnit: self.currencyUnit)
        priceBankRequest.getPriceBanks { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let priceBanks):
                //                print(priceBanks)
                self?.listOfPriceBanks = priceBanks
                
                if self?.listOfPriceBanks.count ?? 0 > 0 {
                    
                    switch sort{
                    case .bankName:
                        self!.listOfPriceBanks.sort{
                            ($0.bankName ?? "").compare(($1.bankName ?? ""), locale: self!.turkish) == .orderedAscending
                        }
                        break
                    case .buyPriceDecreasing:
                        self!.listOfPriceBanks.sort(by: {($0.buyPrice ?? "") > ($1.buyPrice ?? "")})
                        break
                    case .scissorsRange:
//                        if self!.currencyUnit.getCurrencyString() != "gold"{
                            let formatter = NumberFormatter()
                            formatter.decimalSeparator = ","
                            
                            self!.listOfPriceBanks.sort(by: {(formatter.number(from: $0.sellPrice ?? "")!.doubleValue -  formatter.number(from: $0.buyPrice ?? "")!.doubleValue) < (formatter.number(from: $1.sellPrice ?? "")!.doubleValue - formatter.number(from: $1.buyPrice ?? "")!.doubleValue)})
//                        }else{
//                            self?.listOfPriceBanks.sort(by: {(Double($0.sellPrice ?? "0")! - Double($0.buyPrice ?? "")!) < (Double($1.sellPrice ?? "0")! - Double($1.buyPrice ?? "")!)})
//                        }
                        break
                    case .buyPriceGrowing:
                        self!.listOfPriceBanks.sort(by: {$0.buyPrice ?? "" < $1.buyPrice ?? ""})
                        break
                    case .salesPriceDecreasing:
                        self!.listOfPriceBanks.sort(by: {$0.sellPrice ?? "" > $1.sellPrice ?? ""})
                        break
                    case .salesPriceGrowing:
                        self!.listOfPriceBanks.sort(by: {$0.sellPrice ?? "" < $1.sellPrice ?? ""})
                        break
                    }
                    self!.showInterstitial()
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listOfPriceBanks.count + 2
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let cell: BannerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bannerCell", for: indexPath) as? BannerTableViewCell{
                cell.bannerCellView.rootViewController = self
                return cell
            } else {
                return UITableViewCell()
            }
        }else if indexPath.row == listOfPriceBanks.count + 1{
            if let cell: BannerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bannerCell", for: indexPath) as? BannerTableViewCell{
                cell.bannerCellView.rootViewController = self
                return cell
            } else {
                return UITableViewCell()
            }
        }else {
            if let cell: PriceBankTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PriceBankTableViewCell{
                let obje = listOfPriceBanks[indexPath.row - 1]
                cell.bankName.text = obje.bankName
                cell.sellPrice.text = obje.sellPrice
                cell.buyPrice.text = obje.buyPrice
                
                
                return cell
            } else {
                return UITableViewCell()
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(self.listOfPriceBanks[indexPath.row])
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func btnCurrency(_ sender: Any) {
        var sheetAction = UIAlertController()
        if UIDevice.current.userInterfaceIdiom == .pad{
            sheetAction = UIAlertController(title: "Döviz Türü", message: nil, preferredStyle: .alert)
        }else {
            sheetAction = UIAlertController(title: "Döviz Türü", message: nil, preferredStyle: .actionSheet)
        }
//        let sheetAction = UIAlertController(title: "Döviz Türü", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let dollar = UIAlertAction(title: "Dolar", style: .default) { (action) in
            self.currencyUnit = Currency.usd
            self.btnCurrency.image = UIImage(named: "usd")
//            self.getPriceBank(.bankName)
            self.getPriceBank(self.sort)
//            self.showInterstitial()
            
        }
        let euro = UIAlertAction(title: "Euro", style: .default) { (action) in
            self.currencyUnit = Currency.euro
            self.btnCurrency.image = UIImage(named: "euro")
//            self.getPriceBank(.bankName)
            self.getPriceBank(self.sort)
//            self.showInterstitial()
        }
        let gold = UIAlertAction(title: "Altın", style: .default) { (action) in
            self.currencyUnit = Currency.gold
            self.btnCurrency.image = UIImage(named: "gold")
//            self.getPriceBank(.bankName)
            self.getPriceBank(self.sort)
//            self.showInterstitial()
        }
        
//        let crypto = UIAlertAction(title: "Kripto", style: .default) { (action) in
//            self.currencyUnit = Currency.gold
//            self.btnCurrency.image = UIImage(named: "Crypto")
////            self.getPriceBank(.bankName)
//            self.getPriceBank(self.sort)
//            self.showInterstitial()
//        }
        
        let cancel = UIAlertAction(title: "Vazgeç", style: .cancel) { (action) in
            
        }
        sheetAction.addAction(dollar)
        sheetAction.addAction(euro)
        sheetAction.addAction(gold)
        sheetAction.addAction(cancel)
        self.present(sheetAction, animated: true, completion: nil)
    }
    
    @IBAction func btnSort(_ sender: Any) {
        var sheetAction = UIAlertController()
        if UIDevice.current.userInterfaceIdiom == .pad{
            sheetAction = UIAlertController(title: "Filtre", message: nil, preferredStyle: .alert)
        }else {
            sheetAction = UIAlertController(title: "Filtre", message: nil, preferredStyle: .actionSheet)
        }
        
//        let sheetAction = UIAlertController(title: "Filtre", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        let bankName = UIAlertAction(title: "Banka Adına Göre", style: .default) { (action) in
            self.sort = .bankName
            
        }
        let scissorsRange = UIAlertAction(title: "Makas Aralığına Göre", style: .default) { (action) in
            self.sort = .scissorsRange
            
        }
        let buyPriceDecreasing = UIAlertAction(title: "Alış Fiyatına Göre Azalan", style: .default) { (action) in
            self.sort = .buyPriceDecreasing
            
        }
        
        let  buyPriceGrowing = UIAlertAction(title: "Alış Fiyatına Göre Artan", style: .default) { (action) in
            self.sort = .buyPriceGrowing
            
        }
        
        let salesPriceDecreasing = UIAlertAction(title: "Satış Fiyatına Göre Azalan", style: .default) { (action) in
            self.sort = .salesPriceDecreasing
            
        }
        
        let salesPriceGrowing = UIAlertAction(title: "Satış Fiyatına Göre Artan", style: .default) { (action) in
            self.sort = .salesPriceGrowing
            
        }
        
        let cancel = UIAlertAction(title: "Vazgeç", style: .cancel) { (action) in
            
        }
        sheetAction.addAction(bankName)
        sheetAction.addAction(scissorsRange)
        sheetAction.addAction(buyPriceDecreasing)
        sheetAction.addAction(buyPriceGrowing)
        sheetAction.addAction(salesPriceDecreasing)
        sheetAction.addAction(salesPriceGrowing)
        
        
        sheetAction.addAction(cancel)
        self.present(sheetAction, animated: true, completion: nil)
    }
    
    
 
    
    
    

    
}



