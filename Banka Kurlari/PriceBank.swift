//
//  PriceBank.swift
//  Banka Makas Araligi
//
//  Created by MacBook Pro on 24.05.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import Foundation


struct PriceBank:Decodable {
    var GetPriceBankListResult: [PriceBankDetail]
        
}

struct PriceBankDetail:Decodable {
    var bankName:String?
    var buyPrice: String?
    var buyStatus: String?
    var sellPrice: String?
    var sellStatus: String?
    var priceRate: String?
}

enum Currency: Int{
   case usd = 3
    case euro = 2
    case gold = 1
//    case crypto = 4
}

extension Currency{
    func getCurrencyString()-> String{
        switch self.rawValue{
        case 1: return "gold"
        case 2: return "euro"
        case 3: return "usd"
//        case 4: return "crypto"
        default:
            return ""
        }
    }
}
