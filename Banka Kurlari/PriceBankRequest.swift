//
//  PriceBankRequest.swift
//  Banka Makas Araligi
//
//  Created by MacBook Pro on 24.05.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import Foundation

enum PriceBankError:Error {
    case noDataAvailable
    case canNotProcessData
}


struct PriceBankRequest {
    var resourceURL: URL
    
    init(currencyUnit: Currency) {
        
        
//        let resourceString = "http://138.68.103.38:3000/currency_type=\(currencyUnit)"
        let resourceString = "http://bt.imaginativeapps.net/Kurlar.svc//getpricebanklist/\(currencyUnit.rawValue)"
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    func getPriceBanks (completion: @escaping(Result<[PriceBankDetail], PriceBankError>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourceURL) { (data, response, error) in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do{
                let decoder = JSONDecoder()
                let priceBankResponse = try decoder.decode(PriceBank.self, from: jsonData)
                let priceBankDetails = priceBankResponse.GetPriceBankListResult
                completion(.success(priceBankDetails))
            }catch{
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
}
