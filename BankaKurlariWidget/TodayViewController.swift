//
//  TodayViewController.swift
//  BankaMakasAraligiWidget
//
//  Created by MacBook Pro on 25.10.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblDolarBank: UILabel!
    @IBOutlet weak var lblDolarBuy: UILabel!
    @IBOutlet weak var lblDolarSell: UILabel!
    @IBOutlet weak var lblEuroBank: UILabel!
    @IBOutlet weak var lblEuroBuy: UILabel!
    @IBOutlet weak var lblEuroSell: UILabel!
    @IBOutlet weak var lblGoldBank: UILabel!
    @IBOutlet weak var lblGoldBuy: UILabel!
    @IBOutlet weak var lblGoldSell: UILabel!
    
    
    var listOfUsd = [PriceBankDetail]()
    var listOfEuro = [PriceBankDetail]()
    var listOfGold = [PriceBankDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        viewContainer.isHidden = true
        viewTitle.isHidden = true
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        indicator.startAnimating()
        getUsd()
        
        
        
        completionHandler(NCUpdateResult.newData)
    }
    
//    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
//        if activeDisplayMode == .compact {
//            self.preferredContentSize = maxSize
//        } else if activeDisplayMode == .expanded {
//            self.preferredContentSize = CGSize(width: maxSize.width, height: 150)
//        }
//    }

    

    func getUsd(){
        print("getPriceBank")
        let priceBankRequest = PriceBankRequest(currencyUnit: Currency.usd)
        priceBankRequest.getPriceBanks { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let priceBanks):
                DispatchQueue.main.async {
                    self?.listOfUsd = priceBanks
                    self!.getEuro()
                }
                    
            }
        }
    }
    
    func getEuro(){
          print("getPriceBank")
        let priceBankRequest = PriceBankRequest(currencyUnit: Currency.euro)
          priceBankRequest.getPriceBanks { [weak self] result in
              switch result {
              case .failure(let error):
                  print(error)
              case .success(let priceBanks):
                DispatchQueue.main.async {
                    self?.listOfEuro = priceBanks
                    self!.getGold()
                }
                      
              }
          }
      }
    
    func getGold(){
             print("getPriceBank")
        let priceBankRequest = PriceBankRequest(currencyUnit: Currency.gold)
             priceBankRequest.getPriceBanks { [weak self] result in
                 switch result {
                 case .failure(let error):
                     print(error)
                 case .success(let priceBanks):
                         
                         DispatchQueue.main.async {
                            self?.listOfGold = priceBanks
                            self!.load()
                    }
                 }
             }
         }
    
    
    func load(){
        let formatter = NumberFormatter()
        formatter.decimalSeparator = ","
        self.listOfUsd.sort(by: {(formatter.number(from: $0.sellPrice ?? "")!.doubleValue -  formatter.number(from: $0.buyPrice ?? "")!.doubleValue) < (formatter.number(from: $1.sellPrice ?? "")!.doubleValue - formatter.number(from: $1.buyPrice ?? "")!.doubleValue)})
        self.lblDolarBank.text =  self.listOfUsd[0].bankName
        self.lblDolarBuy.text = self.listOfUsd[0].buyPrice
        self.lblDolarSell.text = self.listOfUsd[0].sellPrice
        
        self.listOfEuro.sort(by: {(formatter.number(from: $0.sellPrice ?? "")!.doubleValue -  formatter.number(from: $0.buyPrice ?? "")!.doubleValue) < (formatter.number(from: $1.sellPrice ?? "")!.doubleValue - formatter.number(from: $1.buyPrice ?? "")!.doubleValue)})
        self.lblEuroBank.text =  self.listOfEuro[0].bankName
          self.lblEuroBuy.text = self.listOfEuro[0].buyPrice
         self.lblEuroSell.text = self.listOfEuro[0].sellPrice
        
        self.listOfGold.sort(by: {(formatter.number(from:$0.sellPrice ?? "")!.doubleValue - formatter.number(from:$0.buyPrice ?? "")!.doubleValue) < (formatter.number(from:$1.sellPrice ?? "")!.doubleValue - formatter.number(from:$1.buyPrice ?? "")!.doubleValue)})
        self.lblGoldBank.text =  self.listOfGold[0].bankName
        self.lblGoldBuy.text = self.listOfGold[0].buyPrice
        self.lblGoldSell.text = self.listOfGold[0].sellPrice
        
        
        viewTitle.isHidden = false
        viewContainer.isHidden = false
        indicator.stopAnimating()
        
    }
    
    
}

